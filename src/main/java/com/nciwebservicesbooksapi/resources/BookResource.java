package com.nciwebservicesbooksapi.resources;

import com.nciwebservicesbooksapi.common.ServerConstants;
import com.nciwebservicesbooksapi.model.Book;
import com.nciwebservicesbooksapi.model.ErrorMessage;
import com.nciwebservicesbooksapi.model.SuccessMessage;
import com.nciwebservicesbooksapi.service.BookService;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Expose the BookAPI
 * RESTFUL design.
 * Both XML and JSON are supported, however, JSON is preferred
 * POST is being used for creating to fulfill the specification
 * we considered it as idempotent.
 */
@Path("/books")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class BookResource {

    /**
     * It has to be static, otherwise for different requests when new resource object is created
     * new service would be created which would generate more dummy data to the static inMemory List it has.
     * Hardcoded for now, however, in real-life scenario this would be dependency injected.
     */
    private static final BookService bookService = new BookService();

    @Context
    private UriInfo uriInfo;

    @GET
    public List<Book> list() {
        return bookService.listBooks();
    }

    @GET
    @Path("/{bookId}")
    public Book getById(@PathParam("bookId") int id) {
        return bookService.getBookById(id);
    }

    @POST
    public Response create(Book book) {

        if (book == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_IS_NOT_VALID_CODE,
                            ServerConstants.REQUEST_IS_NOT_VALID_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();
        }

        if (book.getId() != 0) {
            //server is gonna generate id.
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_ID_FOUND_CODE,
                            ServerConstants.REQUEST_ID_FOUND_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();
        }

        bookService.addBook(book);

        URI uri = uriInfo.getAbsolutePathBuilder().path(book.getId() + "").build();
        return Response.created(uri)
                .entity(new SuccessMessage(ServerConstants.SUCCESS_CODE,
                        ServerConstants.SUCCESS_CREATED_TEXT, book))
                .type(MediaType.APPLICATION_JSON).build();
    }

    @PUT
    public Response update(Book book) {

        if (book == null)
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_IS_NOT_VALID_CODE,
                            ServerConstants.REQUEST_IS_NOT_VALID_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();

        try {

            bookService.updateBook(book);

        } catch (NotFoundException e) {

            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_ID_NOT_FOUND_CODE,
                            ServerConstants.REQUEST_ID_NOT_FOUND_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();
        }

        return Response.ok().entity(new SuccessMessage(ServerConstants.SUCCESS_CODE,
                ServerConstants.SUCCESS_UPDATED_TEXT, book))
                .type(MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("{bookId}")
    public Response delete(@PathParam("bookId") int id) {

        Book book = bookService.getBookById(id);

        if (book == null)
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_ID_NOT_FOUND_CODE,
                            ServerConstants.REQUEST_ID_NOT_FOUND_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();

        try {

            bookService.deleteBookById(id);

        } catch (NotFoundException e) {

            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ErrorMessage(ServerConstants.REQUEST_ID_NOT_FOUND_CODE,
                            ServerConstants.REQUEST_ID_NOT_FOUND_TEXT, book))
                    .type(MediaType.APPLICATION_JSON).build();
        }

        return Response.ok().entity(new SuccessMessage(ServerConstants.SUCCESS_CODE,
                ServerConstants.SUCCESS_DELETED_TEXT, book))
                .type(MediaType.APPLICATION_JSON).build();
    }
}
