package com.nciwebservicesbooksapi.service;

import com.nciwebservicesbooksapi.model.Book;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class BookService {

    private static final List<Book> bookList = new ArrayList<>();
    private static long nextId = 0;

    public BookService() {

        /**
         * Create some default data as we use inMemory solution
         * this comes handy to be able to list straightaway
         */
        Date date = new Date();

        Book book1 = new Book();
        book1.setId(getNextId());
        book1.setTitle("First Book");
        book1.setPublisher("FirstOnes");
        book1.setAuthor("Contructor");
        book1.setLastUpdated(date);
        book1.setLastUpdated(date);
        bookList.add(book1);

        Book book2 = new Book();
        book2.setId(getNextId());
        book2.setTitle("Second Book");
        book2.setPublisher("SecondOnes");
        book2.setAuthor("Contructor");
        book2.setLastUpdated(date);
        book2.setLastUpdated(date);
        bookList.add(book2);
    }

    public List<Book> listBooks() {
        return bookList;
    }

    public Book getBookById(final int id) throws NotFoundException {

        Optional<Book> bookOptional = bookList.stream().filter(b -> b.getId() == id).findFirst();

        if (!bookOptional.isPresent()) throw new NotFoundException("Book does not exist with id: " + id);

        return bookOptional.get();
    }

    public void addBook(Book book) {

        book.setId(getNextId());
        book.setCreated(new Date());
        book.setLastUpdated(new Date());
        bookList.add(book);
    }

    public void updateBook(Book book) {

        Book bookToUpdate = getBookById(Math.toIntExact(book.getId()));

        bookToUpdate.setTitle(book.getTitle());
        bookToUpdate.setAuthor(book.getAuthor());
        bookToUpdate.setPublisher(book.getPublisher());
        bookToUpdate.setLastUpdated(new Date());
    }

    public void deleteBookById(int id) throws NotFoundException {

        Book book = getBookById(id);
        bookList.remove(book);
    }

    private long getNextId() {

        nextId++;
        return nextId;
    }
}
