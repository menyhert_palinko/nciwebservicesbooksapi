package com.nciwebservicesbooksapi.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseMessage {

    /**
     * One of the Best practices to have a meaningful code
     * Usually internally standardized within an organization
     */
    private int code;

    /**
     * Error message to show the user what the problem is
     */
    private String message;

    private Book book;

    public ResponseMessage() {
    }

    public ResponseMessage(int code, String message, Book book) {
        this.code = code;
        this.message = message;
        this.book = book;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
