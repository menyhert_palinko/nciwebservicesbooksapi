package com.nciwebservicesbooksapi.model;


/**
 * POJO designed to be serialised to JSON
 * returned in case of success requests
 */
public class SuccessMessage extends ResponseMessage {

    public SuccessMessage() {
    }

    public SuccessMessage(int code, String message, Book book) {
        super(code, message, book);
    }
}
