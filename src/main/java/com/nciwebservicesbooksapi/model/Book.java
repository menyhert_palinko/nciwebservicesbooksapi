package com.nciwebservicesbooksapi.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Book POJO the main model for this project
 * In real world application this could be Entity for persistent
 * or DTO object depending on the design we choose.
 *
 * Here it's a simple POJO which is serialized and desirialized as JSON
 * in order to help demonstrating API development skills.
 */
@XmlRootElement
public class Book {

    private long id;
    private String title;
    private String author;
    private String publisher;
    private Date created;
    private Date lastUpdated;

    public Book() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
