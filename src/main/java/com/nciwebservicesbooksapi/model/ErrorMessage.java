package com.nciwebservicesbooksapi.model;

/**
 * POJO designed to be serialised to JSON
 * returned in case of problems
 */
public class ErrorMessage extends ResponseMessage {

    public ErrorMessage() {
    }

    public ErrorMessage(int code, String message, Book book) {
        super(code, message, book);
    }
}
